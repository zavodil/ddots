DDOTS on Ubuntu
===============

Ubuntu is great for bare metal setup.

(Known issues require Linux Kernel 4.0+ and AUFS support).


Step 1 (Preinstall Dependencies)
--------------------------------

Please, create user `ddots`.

```bash
$ sudo adduser ddots
```

You will need git, make, and the latest Docker.

```bash
$ wget -qO- https://get.docker.com/ | sh
$ sudo usermod -aG docker ddots
## NOTE: Relogin or `su ddots` to reload user groups before continue

$ sudo apt-get install git-core make
```


Step 2 (Install DDOTS)
----------------------

Read more [here](INSTALL_DDOTS.md). (This step is the same for all deployment OSes)



Step 3 (Setup DDOTS autostart)
------------------------------

The latest Ubuntu uses systemd, so here is the systemd unit config:

```bash
$ sudo tee /etc/systemd/system/ddots.service <<'EOF'
[Unit]
Description=Dockerized Distributed Olymiad Testing System
After=docker.service
Requires=docker.service

[Service]
WorkingDirectory=/home/ddots/ddots

User=ddots
Group=ddots

Restart=always
RemainAfterExit=yes
ExecStartPre=-/usr/bin/make update
ExecStartPre=-/usr/bin/make stop
ExecStartPre=-/usr/bin/make clean_dangling_docker_images
ExecStart=/usr/bin/make start
ExecStop=/usr/bin/make stop
TimeoutSec=3600

[Install]
WantedBy=multi-user.target
EOF
```

Enable the service:

```bash
$ sudo systemctl enable ddots
$ sudo systemctl start ddots
```


Done
----

We are all set! You can check the logs using DDOTS provided `make`-command:

```bash
$ make logs
```

OR using `journalctl`:

```bash
$ sudo journalctl --follow --unit ddots
```
