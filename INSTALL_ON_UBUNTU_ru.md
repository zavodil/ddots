DDOTS on Ubuntu
===============

Ubuntu - отличный вариант для установки "на железо".

(Известные проблемы были решены с использованием Linux Kernel 4.0+ с поддержкой AUFS, так что
Ubuntu 16.04+ должен работать без проблем)


Шаг 1 (Предустановка зависимостей)
----------------------------------

Пожалуйста, создайте пользователя `ddots`:

```bash
$ sudo adduser ddots
```

Также, понадобятся git, make, и последняя версия Docker.

```bash
$ wget -qO- https://get.docker.com/ | sh
$ sudo usermod -aG docker ddots
## ВАЖНО: Перелогиньтесь или `su ddots` перед тем как продолжить!

$ sudo apt-get install git-core make
```


Шаг 2 (Установка DDOTS)
-----------------------

Этот шаг идентичен для всех ОС, поэтому вынесен в отдельный файл [инструкции](INSTALL_DDOTS.md).


Шаг 3 (Настройка автозапуска DDOTS)
-----------------------------------

Последние версии Ubuntu используют systemd, так что добавим systemd unit-конфигурацию:

```bash
$ sudo tee /etc/systemd/system/ddots.service <<'EOF'
[Unit]
Description=Dockerized Distributed Olymiad Testing System
After=docker.service
Requires=docker.service

[Service]
WorkingDirectory=/home/ddots/ddots

User=ddots
Group=ddots

Restart=always
RemainAfterExit=yes
ExecStartPre=-/usr/bin/make update
ExecStartPre=-/usr/bin/make stop
ExecStartPre=-/usr/bin/make clean_dangling_docker_images
ExecStart=/usr/bin/make start
ExecStop=/usr/bin/make stop
TimeoutSec=3600

[Install]
WantedBy=multi-user.target
EOF
```

Включаем DDOTS сервис:

```bash
$ sudo systemctl enable ddots
$ sudo systemctl start ddots
```


Готово!
-------

Всё готово! Проверить логи можно воспользовавшись `make`-командой:

```bash
$ make logs
```

ИЛИ используя `journalctl`:

```bash
$ sudo journalctl --follow --unit ddots
```
