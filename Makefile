DDOTS_CPU_COUNT ?= $(shell grep -c processor /proc/cpuinfo)
DDOTS_SANDBOX_SIZE ?= 100m
DDOTS_TMP_SIZE ?= 100m
DDOTS_DATA_ROOT ?= /data
DDOTS_SANDBOX_ROOT ?= /sandbox

START_DETACHED = true

DOCKER_START_EXTRA_ARGUMENTS ?=
DOCKER_SOCK_PATH ?= /var/run/docker.sock

DDOTS_CPU_IDS = $(shell seq 0 $$(( $(DDOTS_CPU_COUNT) - 1 )))
DDOTS_INSTANCES = $(DDOTS_CPU_IDS) testing test_solution

TESTING_EXTRA_ARGUMENTS ?= --log-cli-level=INFO --cov=worker --cov-report=term-missing

# make logs uses trap, which doesn't work in dash;
# we also target bash in echo statements, though it is not that critical
SHELL := bash

COLOR_GREEN := \033[1;32m
COLOR_WHITE := \033[1;37m
COLOR_NONE := \033[0m

define echo
	echo $$'$(COLOR_GREEN)'$1$$'$(COLOR_NONE)'
endef


.PHONY: install
install: ddots_compilers ddots_runners

.PHONY: update
update:
	git checkout .
	git pull
	git submodule update --init --recursive
	$(MAKE) --jobs install

.PHONY: ddots_compilers
ddots_compilers:
	$(MAKE) --jobs=6 --directory=./ddots_compilers

.PHONY: ddots_runners
ddots_runners:
	$(MAKE) --jobs=6 --directory=./ddots_runners

.PHONY: clean_dangling_docker_images
clean_dangling_docker_images: $(DOCKER_SOCK_PATH)
	docker rm `docker ps -a -q` || true
	docker rmi `docker images -q --filter=dangling=true` || true

.PHONY: build
build: $(DOCKER_SOCK_PATH)
	@$(call echo,"Building DDOTS...")
	sed "s/#ENV DOCKER_GID=X/ENV DOCKER_GID=`stat -c %g $(DOCKER_SOCK_PATH)`/" ./Dockerfile > ./.Dockerfile.swp
	docker build --tag ddots-testing-system --file .Dockerfile.swp .
	#docker build --pull --tag ddots-testing-system --file .Dockerfile.swp .
	rm ./.Dockerfile.swp
	@$(call echo,"DDOTS has been built successfully")

.PHONY: build-tests
build-tests: $(DOCKER_SOCK_PATH) build
	@$(call echo,"Building DDOTS tests environment...")
	docker build --tag ddots-testing-system-tests ./testing_system/tests
	@$(call echo,"DDOTS tests environment has been built successfully")

# This Makefile magic is nicely described here:
# http://stackoverflow.com/a/12110773/1178806
#
DDOTS_CLEANS = $(addprefix clean-,$(DDOTS_INSTANCES))
.PHONY: $(DDOTS_CLEANS)
$(DDOTS_CLEANS): clean-%:
	docker ps --all --quiet --filter volume='ddots-testing-system-$*-data' | xargs --no-run-if-empty docker rm
	- docker volume rm 'ddots-testing-system-$*-data'
	! docker volume ls | grep 'ddots-testing-system-$*-data'
	docker ps --all --quiet --filter volume='ddots-testing-system-$*-sandbox' | xargs --no-run-if-empty docker rm
	- docker volume rm 'ddots-testing-system-$*-sandbox'
	! docker volume ls | grep 'ddots-testing-system-$*-sandbox'

.PHONY: clean
clean: $(addprefix clean-,$(DDOTS_INSTANCES))

DDOTS_STARTS = $(addprefix start-,$(DDOTS_INSTANCES))
.PHONY: $(DDOTS_STARTS)
$(DDOTS_STARTS): start-%: $(DOCKER_SOCK_PATH) build clean-%
	docker volume create \
		--name 'ddots-testing-system-$*-data'
	docker volume create \
		--opt type=tmpfs \
		--opt device=tmpfs \
		--opt 'o=size=$(DDOTS_SANDBOX_SIZE),uid=11,mode=755' \
		--name 'ddots-testing-system-$*-sandbox'
	set -x ; \
	if [ '$(START_DETACHED)' = 'true' ] && [ '$*' != 'testing' ] && [ '$*' != 'test_solution' ]; then \
		DOCKER_START_EXTRA_ARGUMENTS=--detach ; \
	fi ; \
	START_DOCKER_IMAGE="ddots-testing-system" ; \
	if [ '$*' = 'testing' ]; then \
		START_DOCKER_IMAGE+="-tests" ; \
	fi ; \
	START_COMMAND="/usr/bin/python3 ./manage.py start" ; \
	if [ '$*' = 'testing' ]; then \
		START_COMMAND="py.test -p no:cacheprovider $(TESTING_EXTRA_ARGUMENTS)" ; \
	fi ; \
	if [ '$*' = 'test_solution' ]; then \
		START_COMMAND="/usr/bin/python3 ./manage.py test_solution" ; \
	fi ; \
	docker run $$DOCKER_START_EXTRA_ARGUMENTS \
		--name 'ddots-testing-system-$*' \
		--log-opt max-size=5m \
		--log-opt max-file=100 \
		--memory '128m' \
		--memory-swap '128m' \
		--volume '$(DOCKER_SOCK_PATH):$(DOCKER_SOCK_PATH)' \
		--volume 'ddots-testing-system-$*-data:$(DDOTS_DATA_ROOT)' \
		--volume 'ddots-testing-system-$*-sandbox:$(DDOTS_SANDBOX_ROOT)' \
		--env 'DOCKER_SOCK_PATH=$(DOCKER_SOCK_PATH)' \
		--env 'DDOTS_CPU_ID=$*' \
		--env 'DDOTS_BASE_CONTAINER_NAME=$*' \
		--env "DOTS_API_URL=$$DOTS_API_URL" \
		--env "DOTS_USERNAME=$$DOTS_USERNAME" \
		--env "DOTS_PASSWORD=$$DOTS_PASSWORD" \
		$$START_DOCKER_IMAGE \
		$$START_COMMAND
	@$(call echo,'Container ddots-testing-system-$* has started')

.PHONY: start
start: $(addprefix start-,$(DDOTS_CPU_IDS))
	@$(call echo,$$'DDOTS has started. Check logs by running: "$(COLOR_WHITE)make logs$(COLOR_GREEN)" or "$(COLOR_WHITE)docker logs ddots-testing-system-ID$(COLOR_GREEN)"')

DDOTS_STOPS = $(addprefix stop-,$(DDOTS_INSTANCES))
.PHONY: $(DDOTS_STOPS)
$(DDOTS_STOPS): stop-%:
	@$(call echo,"Stopping DDOTS #$*...")
	- docker stop 'ddots-testing-system-$*'
	- docker rm 'ddots-testing-system-$*'
	! docker ps -a | grep -P 'ddots-testing-system-$*$$'

.PHONY: stop
stop: $(addprefix stop-,$(DDOTS_CPU_IDS))

.PHONY: restart
restart: stop start

.PHONY: logs
logs: $(DOCKER_SOCK_PATH)
	@trap 'kill $$(jobs -p) &>/dev/null' 0 1 2 3 6 15 ; \
	for CPU_ID in `seq 0 $$(( $(DDOTS_CPU_COUNT) - 1 ))`; do \
		docker logs --timestamps --tail 5 --follow \
			ddots-testing-system-$$CPU_ID 2>&1 | sed $$'s/^/\x1b[1;33mDDOTS\x1b[1;31m #'$$CPU_ID$$'\x1b[0m: /;s/ERROR.*/\x1b[1;31m&\x1b[0m/' & \
	done ; \
	wait

.PHONY: test
test: stop-testing build-tests start-testing stop-testing clean-testing
	@$(call echo,"DDOTS tests passed!")

.PHONY: test_solution
test_solution: $(DOCKER_SOCK_PATH) build
	@$(call echo,"Running DDOTS to test a solution...")
	DOCKER_START_EXTRA_ARGUMENTS="--env PROGRAMMING_LANGUAGE=$$PROGRAMMING_LANGUAGE --env TESTING_MODE=$$TESTING_MODE --volume $$SOLUTION:/tmp/ddots-test_solution/solution.source --volume $$PROBLEM_ROOT:/tmp/ddots-test_solution/problem" \
		$(MAKE) start-test_solution
