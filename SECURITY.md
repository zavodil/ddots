Security Notes
==============

Write Protection
----------------

As we have decided to use a batch mode to run solutions, we have to avoid any data propagation
between runs. To minimize possible harm from the user solutions we have to run Docker containers
in the read-only mode, so user can only write to mounted volumes.

* use two users:
    * `operator`, to run a batch execution, control Time Limit and make a report on Memory Peak of a given solution run;
    * `nobody`, to run a given solution on a given test.
* setup two volumes (mount points for Docker):
    * one for a solution and a pack of tests (for `operator` only);
    * second for a solution run sandbox (the only path, where `nobody` can write a file).

```bash
$ docker run \
    --read-only \
    --user=operator \
    --volume=./solution_and_tests:/home/operator \
    --volume=./sandbox:/home/nobody \
    <docker_image>
```


Memory Limits
-------------

The first attempt will be to use Docker's built-in capabilities to set memory limits. This solution
has one major drawback: OOM will kill the solution, but it is hard to distinguish whether it was TL
or a crash.

Another way to address this is no use `setrlimit()` inside of a container.

```bash
$ docker run \
    --memory='64m' \
    --memory-swap='-1' \
    <docker_image>
```

WARNING: Docker's memory limits are not applied when the proposed cgmemtime utility works. For more
details read 'Memory Peak and Execution Time Measurement' section. However, I suggest we still
use Docker's limits as a just-in-case solution.

UPDATE: `cgroups` configuration was implemented in pure Rust in `ddots-runner` which works
flawlessly.

Time Limits
-----------

At this point there is no clear 'fair' solution of how to set time limits. The issue here is that
we may test solutions on any kind of a machine, e.g., a powerful server, or an Atom-based netbook.
One second real time is very different for those two machines in the example; one solution may pass
or fail with TL just by comming to different testers.

Relying on a real time timer, i.e. `sleep(1)`, we face the following issues:

* unfair TL on 'slow' vs 'fast' testing machines;

Relying only on a CPU time (jiffies/tick counts/etc), we face:

* `sleep(10000)`, io wait (`read(/dev/zero)`), other infinite-wait operations - a solution
that doesn't use CPU at all, so we never kill it by TL;

Thus, we have to combine both timeout approaches in a best way.

The best solution to this issue that we have came up with is to use a time factor multiplier (TFM),
which is going to be calculated by a 'standard' 1-second-running program, and retesting on another
machine to confirm TL:

Set hard time limit to `2*(TL*TFM)` and see how it goes:

* IF the solution takes less than `1*(TL*TFM)` to run - it is OK
* IF the solution takes longer than `2*(TL*TFM)` - it a true TL
* IF the solution execution time is between `1*(TL*TFM)` and `2*(TL*TFM)` - send the solution back
and a second server will make a final decision based on `1*(TL*TFM)` hard limit.

['standard' 1-second-running program](https://github.com/qbit-org-ua/ddots-one-second-standard)
did not play that well (different types of workloads produce different results), but it is better
than manual setting the TFM (time factor multiplier) out of thin air.


Memory Peak and Execution Time Measurement
------------------------------------------

[cgmemtime](https://github.com/gsauthof/cgmemtime) is another awesome project which emerged from
the cgroups Kernel feature.

```bash
$ sudo ./cgmemtime --setup -g <myusergroup> --perm 775

$ ./cgmemtime ./testa x 10 20 30
Parent PID is 27189
Allocating 10 MiBs
New Child: 27193
Allocating 20 MiBs
New Child: 27194
Allocating 30 MiBs
Child user:    0.000 s
Child sys :    0.005 s
Child wall:    6.006 s
Child high-water RSS                    :      11648 KiB
Recursive and acc. high-water RSS+CACHE :      61840 KiB

$ ./cgmemtime python -c 'print range(100000)[48517]'
48517
Child user:    0.014 s
Child sys :    0.014 s
Child wall:    0.029 s
Child high-water RSS                    :       9948 KiB
Recursive and acc. high-water RSS+CACHE :       5724 KiB
```

I have tested it with Docker and here is the scenario:

```bash
$ sudo ./cgmemtime --setup -u nobody -g nogroup --perm 775
$ docker run -it --rm -v /tmp/:/tmp/ -v /sys/fs/cgroup:/sys/fs/cgroup:ro --user nobody <docker_image> bash

bash-4.3$ /tmp/cgmemtime/cgmemtime id
uid=65534(nobody) gid=65534(nobody)
Child user:    0.000 s
Child sys :    0.001 s
Child wall:    0.001 s
Child high-water RSS                    :        852 KiB
Recursive and acc. high-water RSS+CACHE :        128 KiB

bash-4.3$ /tmp/cgmemtime/cgmemtime_alpine -t id
uid=65534(nobody) gid=65534(nobody)
0;0.001141;0.001321;780;128
```

NOTE: UID and GID has to be the same inside and outside of the Docker (e.g. in Ubuntu `nobody` user
has `65534` UID and `nogroup` is `65534`, but in Alpine Linux, which we use in our Docker images,
there is no `nogroup`, but there is `nobody` group which has the same GID `65534`, so we are fine!).

WARNING: Another round of experiments revealed that docker's memory limit does not apply to the
process that runs via cgmemtime as it creates another cgroup that does not aware of Docker's memory
limit. The solution is to set this limit again in cgmemtime.c! So we will have to fork it and add
this feature.

UPDATE: `cgmemtime` ideas were reimplemented in pure Rust in `ddots-runner` which works flawlessly.

CPU Limits
----------

Docker provides an easy way to lock a container to a specific CPU core:

```bash
$ docker run \
    --cpuset-cpus=0 \
    <docker_image>
```

So we have to pass a CPU id into each Testing System container, so it can lock all solutions on one CPU.


Fork-bomb Protection
--------------------

```bash
$ bash -c ':(){ :|: & };:'
```

NOTE: Don't run it in uncontrolled environment as it will hung up your machine!

There is no Docker way to prevent fork-bombs correctly. Nevertheless, we use
`cgmemtime` to set memory limits (as it creates separate cgroup, not inherited
from Docker), so we have added another option (`--kernel-memory-limit`) to set
`memory.kmem.limit_in_bytes`.

```bash
$ ./cgmemtime --kernel-memory-limit 1048576 bash -c ':(){ :|: & };:'
```

UPDATE: `cgmemtime` ideas were reimplemented in pure Rust in `ddots-runner` which works flawlessly.

### Old approach (works fine for one container):

* requires Docker 1.6.0+;
* protection works only for non-root users (`docker run --user=nobody` or `su`/`sudo` inside of a container);
* `docker run --ulimit='nproc=5'` limits a number of processes allowed to the user to run simuntaniously.

```bash
$ docker run \
    --user=nobody \
    --limit='nproc=5' \
    <docker_image>
```

Unfortunately, ulimit applies limits per UID instead of per container.
