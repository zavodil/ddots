# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_dotnet_csharp_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.cs')
    OK_solution_path.write(
            'using System.IO;'
            'class Program {'
            '    static void Main(string[] args) {'
            '        StreamWriter sw = new StreamWriter("output.txt");'
            '        sw.Write("Hello World!");'
            '        sw.Close();'
            '    }'
            '}'
        )
    testing_report = compiler_runner(
            'dotnet-csharp',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_dotnet_csharp_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.cs')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'dotnet-csharp',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    print(str(CE.value))
    assert 'solution.source(1,3): error CS1733: Expected expression' in str(CE.value)
