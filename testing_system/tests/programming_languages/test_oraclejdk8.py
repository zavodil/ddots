# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_oraclejdk8_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.java')
    OK_solution_path.write(
            'import java.io.File;'
            'import java.io.RandomAccessFile;'
            'public class Main {'
            '    public static void main(String[] args) {'
            '        try (RandomAccessFile output = new RandomAccessFile('
            '               new File("output.txt"), "rw")) {'
            '            output.writeBytes("Hello World!");'
            '        } catch (Exception e) { e.printStackTrace(); }'
            '    }'
            '}'
        )
    testing_report = compiler_runner(
            'oraclejdk8',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_oraclejdk8_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.java')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'oraclejdk8',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert (
            'solution.source: Testing System Compilation Error: There must be a class named `Main`'
            in str(CE.value)
        )


def test_oraclejdk8_RE(compiler_runner, tmpdir, hello_world_problem):
    """
    The following solution has to raise RE due to the following error:

    >  Error: Main method not found in class Main, please define the main method as:
    >    public static void main(String[] args)
    >  or a JavaFX application class must extend javafx.application.Application
    """

    RE_solution_path = tmpdir.join('RE_solution.java')
    RE_solution_path.write(
            'public class Main {'
            '    public static void main() {'
            '    }'
            '}'
        )
    testing_report = compiler_runner(
            'oraclejdk8',
            problem_path=hello_world_problem['path'],
            solution_source_path=str(RE_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'RE', '0']
