# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_ruby_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.rb')
    OK_solution_path.write('File.open("output.txt", "w").puts("Hello World!")')
    testing_report = compiler_runner(
            'ruby',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_ruby_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.rb')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'ruby',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source:1: syntax error, unexpected end-of-input' in str(CE.value)


def test_ruby_TL(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.rb')
    OK_solution_path.write('while true; end')
    testing_report = compiler_runner(
            'ruby',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'TL', '0']
