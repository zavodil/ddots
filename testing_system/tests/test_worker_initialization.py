from worker import create_app


def test_create_app():
    from config import TestingConfig
    app = create_app(TestingConfig)

    assert hasattr(app, 'run')
