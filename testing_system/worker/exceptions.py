
class TestingSystemRestartSignal(Exception):
    pass


class TestingSystemError(Exception):
    pass


class DockerError(Exception):
    pass


class DockerTimeout(DockerError):
    pass


class ContainerTimeout(DockerError):
    pass


class CompilationError(Exception):
    pass
