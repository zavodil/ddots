# encoding: utf-8
# pylint: disable=invalid-name,wrong-import-position

from .compilers import CompilersManager
compilers_manager = CompilersManager()

from .runners import RunnersManager
runners_manager = RunnersManager()

from .api import DOTSApi
dots_api = DOTSApi()

from .problems import ProblemsManager
problems_manager = ProblemsManager()


def init_app(app):
    for extension in (
            compilers_manager,
            runners_manager,
            dots_api,
            problems_manager,
        ):
        extension.init_app(app)
