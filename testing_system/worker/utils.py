# encoding: utf-8

import asyncio
import functools
import logging

import lockfile
import requests


log = logging.getLogger(__name__)  # pylint: disable=invalid-name


def log_response(logger, msg, response, *args, **kwargs):
    logger(msg + " Response details:\n"
            "    STATUS_CODE=%s\n"
            "    HEADERS=%s\n"
            "    CONTENT=%s",
        *(args + (
            response.status_code,
            response.headers,
            response.content
        )),
        **kwargs
    )


def download_file(
        url, local_filepath,
        chunk_size=1024*512, lock_timeout=10, http_timeout=None, session_get=None, force_etag=False
    ):
    log.debug("Preparing for a file download from '%s' to '%s'", url, local_filepath)
    lock = lockfile.LockFile(local_filepath)
    try:
        lock.acquire(timeout=lock_timeout)
    except lockfile.LockTimeout:
        log.info(
                "File '%s' is locked. Probably, another instance is still downloading it.",
                local_filepath
            )
        raise
    try:
        headers = {}

        etag_filepath = local_filepath.parent / ('%s.etag' % local_filepath.name)
        if force_etag or local_filepath.exists():
            try:
                headers['If-None-Match'] = '"%s"' % etag_filepath.read_text()
            except IOError:
                pass

        if session_get is None:
            session_get = requests.get

        log.debug("Downloading a file from '%s' to '%s'", url, local_filepath)
        response = session_get(url, stream=True, timeout=http_timeout, headers=headers)

        if response.status_code == 304:
            log.debug("File '%s' matches the existing one based on ETag", local_filepath)
            return False
        if response.status_code != 200:
            log_response(log.error, "Download from '%s' has failed" % url, response)
            raise response.raise_for_status()

        log.info("Downloading a new file from '%s' to '%s'", url, local_filepath)
        with open(local_filepath, 'wb') as save_as_file:
            for chunk in response.iter_content(chunk_size=chunk_size):
                # filter out keep-alive new chunks
                if chunk:
                    save_as_file.write(chunk)

        etag = response.headers.get('ETag')
        if etag:
            try:
                etag_filepath.write_text(etag.strip('"'))
            except IOError:
                pass

        log.debug("File '%s' has been downloaded", local_filepath)
        return True
    finally:
        lock.release()


def force_async(func):
    @functools.wraps(func)
    async def wrapper(*args, **kwargs):
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(None, lambda: func(*args, **kwargs))

    return wrapper
